/**
 * @author Massimo De Marchi
 * @created 9/24/15
 *
 * Copyright inkOfPixel 2015.
 */

var mongoose = require('mongoose');

var HotSpotSchema = new mongoose.Schema({
	name: String,
	lat: Number,
	lng: Number,
	prefs: { type: Number, min: 0 }
});

module.exports = mongoose.model('HotSpot', HotSpotSchema);