/**
 * @author Massimo De Marchi
 * @created 9/7/15
 *
 * Copyright inkOfPixel 2015.
 */

var mongoose = require('mongoose');

var SurveySchema = new mongoose.Schema({
	gender: String,
	email: String,
	age: { type: Number, min: 0, max: 120 },
	placeOfOrigin: {
		description: String,
		placeId: String
	},
	startup: {
		location: {
			description: String,
			placeId: String,
			lat: Number,
			lng: Number
		},
		category: String,
		factors: {
			accessToTalent: {type: Number, min: 0, max: 5},
			accessToCapital: {type: Number, min: 0, max: 5},
			burnrate: {type: Number, min: 0, max: 5},
			ecosystem: {type: Number, min: 0, max: 5}
		}
	},
	hotSpots: [String],
	citiesFactors: {
		Copenhagen: {
			accessToTalent: {type: Number, min: 0, max: 5},
			accessToCapital: {type: Number, min: 0, max: 5},
			burnrate: {type: Number, min: 0, max: 5},
			ecosystem: {type: Number, min: 0, max: 5}
		},
		Dublin: {
			accessToTalent: {type: Number, min: 0, max: 5},
			accessToCapital: {type: Number, min: 0, max: 5},
			burnrate: {type: Number, min: 0, max: 5},
			ecosystem: {type: Number, min: 0, max: 5}
		},
		Manchester: {
			accessToTalent: {type: Number, min: 0, max: 5},
			accessToCapital: {type: Number, min: 0, max: 5},
			burnrate: {type: Number, min: 0, max: 5},
			ecosystem: {type: Number, min: 0, max: 5}
		},
		Stockholm: {
			accessToTalent: {type: Number, min: 0, max: 5},
			accessToCapital: {type: Number, min: 0, max: 5},
			burnrate: {type: Number, min: 0, max: 5},
			ecosystem: {type: Number, min: 0, max: 5}
		},
		Munich: {
			accessToTalent: {type: Number, min: 0, max: 5},
			accessToCapital: {type: Number, min: 0, max: 5},
			burnrate: {type: Number, min: 0, max: 5},
			ecosystem: {type: Number, min: 0, max: 5}
		},
		Milan: {
			accessToTalent: {type: Number, min: 0, max: 5},
			accessToCapital: {type: Number, min: 0, max: 5},
			burnrate: {type: Number, min: 0, max: 5},
			ecosystem: {type: Number, min: 0, max: 5}
		},
		Vienna: {
			accessToTalent: {type: Number, min: 0, max: 5},
			accessToCapital: {type: Number, min: 0, max: 5},
			burnrate: {type: Number, min: 0, max: 5},
			ecosystem: {type: Number, min: 0, max: 5}
		}
	}
});

module.exports = mongoose.model('Survey', SurveySchema);












