/**
 * @author Massimo De Marchi
 * @created 9/16/15
 *
 * Copyright inkOfPixel 2015.
 */

var startupMap;
var mapWrapper;
var flowsLayer;
var citiesLayer;

var ratio = 4/3;
var actualRatio = ratio;
var width;
var height;

var projection;

var flowsJSON = null;
var worldJSON = null;
var citiesJSON = null;

var mapRendered = false;


function init() {
	startupMap = d3.select("#heat-map");
	var svg = startupMap.select("#visualization");
	mapWrapper = svg
		.attr("width", width)
		.attr("height", height)
		.append("g").classed("map", true);

	flowsLayer = svg.append("g").classed("flows-layer", true);
	citiesLayer = svg.append("g").classed("cities-layer", true);

	layout();

	loadMap(function(error, geoJSON) {
		if(error) {console.log(error); return}

		worldJSON = geoJSON;
		renderMap();
		mapRendered = true;
		if(flowsJSON) {
			renderFlows();
		}
		if(citiesJSON) {
			renderCities();
		}
	});

	loadFlows(function(error, json) {
		if(error) {console.log(error); return}

		flowsJSON = json;
		if(mapRendered) {
			renderFlows();
		}
	});

	loadCities(function(error, json) {
		if(error) {console.log(error); return}

		citiesJSON = json;
		if(mapRendered) {
			renderCities();
		}
	});
}

function layout() {
	width = startupMap.node().offsetWidth;
	height = Math.min(width / ratio, window.innerHeight);
	actualRatio = width / height;
	startupMap.style("height", height + "px");

	// Update map projection
	projection = d3.geo.mercator()
		.translate([width / 2, height / 2])
		.scale((width - 1) * 3 / (actualRatio * Math.PI))
		.center([-5.225834, 52.432540]);//-10, 58]);
}

function renderMap() {
	var zoom = d3.behavior.zoom()
		.scaleExtent([1, 8])
		.on("zoom", zoomed);

	var path = d3.geo.path()
		.projection(projection);


	var europe = mapWrapper.selectAll(".europe")
		.data([topojson.merge(worldJSON, worldJSON.objects.countries.geometries.filter(function(a) {
			return europeanCountries[a.id];
		}))])
		.attr("d", path);

	europe.enter().append("path")
		.classed("europe", true)
		.attr("d", path);

	europe.exit().remove();

	var notEurope = mapWrapper.selectAll(".not-europe")
		.data([topojson.merge(worldJSON, worldJSON.objects.countries.geometries.filter(function(a) {
			return !europeanCountries[a.id];
		}))])
		.attr("d", path);

	notEurope.enter().append("path")
		.classed("not-europe", true)
		.attr("d", path);

	notEurope.exit().remove();

	var europeanBoundaries = mapWrapper.selectAll(".europe-boundaries")
		.data([topojson.mesh(worldJSON, worldJSON.objects.countries, function(a, b) {
			return a !== b && (europeanCountries[a.id] || europeanCountries[b.id]);
		})])
		.attr("d", path);

	europeanBoundaries.enter().append("path")
		.classed("europe-boundaries", true)
		.attr("d", path);

	europeanBoundaries.exit().remove();


	function zoomed() {
		mapWrapper.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
	}

	// TODO ?
	d3.select(self.frameElement).style("height", height + "px");
}


function renderFlows() {
	var maxFlow = 10000;
	var flowsNumber = Math.min(flowsJSON.length , maxFlow);
	//var opacityScale = d3.scale.pow().exponent(.02).domain([0, 100000]).range([0.8, 0.001]);

	var clutterPoint = 100;
	var clutterPointOpacity = 0.15;

	var minOpacity = 0.001;

	var opacityScale;

	if(flowsNumber < clutterPoint) {
		 opacityScale = d3.scale.linear().domain([0, clutterPoint]).range([0.8, clutterPointOpacity]);
	} else {
		opacityScale = d3.scale.pow().exponent(1.1).domain([clutterPoint, maxFlow]).range([clutterPointOpacity, minOpacity]);
	}

	var flowOpacity = opacityScale(flowsNumber);

	var gFlow = flowsLayer.selectAll(".flow").data(flowsJSON);

	// Update
	gFlow
		.style('opacity', flowOpacity)
		.each(function(flow) {
			var flowGroup = d3.select(this);
			var origin = projection([flow[0].lng, flow[0].lat]);
			origin.x = origin[0];
			origin.y = origin[1];
			var destination = projection([flow[1].lng, flow[1].lat]);
			destination.x = destination[0];
			destination.y = destination[1];

			var angle = computeAngle(origin, destination);
			var distance = computeDistance(origin, destination);
			var transform = 'translate(' + origin.x + ' ' + origin.y + ')';
			transform += 'rotate(' + angle + ')';

			flowGroup
				.attr('transform', transform)
				.select('use')
				.attr('width', distance);
		});

	// Create
	gFlow.enter()
		.append('g')
		.style('opacity', flowOpacity)
		.classed('flow', true)
		.each(function(flow) {
			var flowGroup = d3.select(this);
			var origin = projection([flow[0].lng, flow[0].lat]);
			origin.x = origin[0];
			origin.y = origin[1];
			var destination = projection([flow[1].lng, flow[1].lat]);
			destination.x = destination[0];
			destination.y = destination[1];

			var angle = computeAngle(origin, destination);
			var distance = computeDistance(origin, destination);
			var transform = 'translate(' + origin.x + ' ' + origin.y + ')';
			transform += 'rotate(' + angle + ')';

			flowGroup
				.attr('transform', transform)
				.append('use')
				.attr('x', 0)
				.attr('y', -3)
				.attr('height', 6)
				.attr('width', distance)
				.attr('xlink:href', '#flowsvg');
		});

	// Delete
	gFlow.exit().remove();
}


function renderCities() {
	var cityRadius = 3;

	var cities = citiesJSON.map(function(city) {
		var point = projection([city.lng, city.lat]);
		return {x: point[0], y: point[1], name: city.name, prefs: city.prefs};
	});

	var minPrefs = d3.min(cities, function(city) {return city.prefs});
	var maxPrefs = d3.max(cities, function(city) {return city.prefs});
	var textScale = d3.scale.linear().domain([minPrefs, maxPrefs]).range([1, 1.6]);

	var textOpacityScale = d3.scale.linear().domain([minPrefs, maxPrefs]).range([0.3, 0.8]);

	var citiesGroup = citiesLayer.selectAll('.city').data(cities);

	// Update
	citiesGroup
		.style('opacity', function(d) {return textOpacityScale(d.prefs)})
		.attr('transform', function(d) {
			return 'translate(' + d.x + ' ' + d.y + ')';
		});

	// Enter
	citiesGroup.enter()
		.append('g')
		.classed('city', true)
		.style('opacity', function(d) {return textOpacityScale(d.prefs)})
		.attr('transform', function(d) {
			return 'translate(' + d.x + ' ' + d.y + ')';
		})
		.each(function(d) {
			var cityGroup = d3.select(this);

			cityGroup
				.append('circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', cityRadius);

			var textTransform = 'translate(0 ' + (-(cityRadius +3)) + ')';
			textTransform += ' scale(' + textScale(d.prefs) + ')';

			cityGroup
				.append('g')
				.attr('transform', textTransform)
					.append('text')
					.classed('city-name', true)
					.attr('text-anchor', 'middle')
					.attr('dy', '0')
					.text(function(d) {return d.name});
		});

	// Remove
	citiesGroup.exit().remove();
}

//	<polygon fill="url(#SVGID_1_)" points="0,0 100,-1 100,1"/>

function computeAngle(p1, p2) {
	return (Math.atan2(p1.y - p2.y, p1.x - p2.x) + Math.PI) * 180.0 / Math.PI;
}

function computeDistance(p1, p2) {
	return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
}


// DATA

function loadMap(callback) {
	d3.json("/data/world.json", function(error, world) {
		callback(error, world);
	});
}

function loadFlows(callback) {
	d3.json("/api/flows", function(error, flows) {
		callback(error, flows);
	});
}

function loadCities(callback) {
	d3.json("/api/hotspots", function(error, cities) {
		callback(error, cities);
	});
}


var europeanCountries = {
	8: "Albania",
	20: "Andorra",
	51: "Armenia",
	40: "Austria",
	112: "Belarus",
	56: "Belgium",
	70: "Bosnia and Herzegovina",
	100: "Bulgaria",
	196: "Cyprus",
	191: "Croatia",
	203: "Czech Republic",
	208: "Denmark",
	233: "Estonia",
	246: "Finland",
	250: "France",
	268: "Georgia",
	276: "Germany",
	300: "Greece",
	348: "Hungary",
	352: "Iceland",
	372: "Ireland",
	380: "Italy",
	"-99": "Kosovo",
	428: "Latvia",
	440: "Lithuania",
	442: "Luxembourg",
	807: "Macedonia",
	470: "Malta",
	498: "Moldova",
	499: "Montenegro",
	528: "Netherlands",
	578: "Norway",
	616: "Poland",
	620: "Portugal",
	642: "Romania",
	643: "Russia",
	688: "Serbia",
	703: "Slovakia",
	705: "Slovenia",
	724: "Spain",
	752: "Sweden",
	756: "Switzerland",
	792: "Turkey",
	804: "Ukraine",
	826: "United Kingdom"
};

window.onresize = function() {
	layout();
	if(worldJSON) {
		renderMap();
		if(flowsJSON) {
			renderFlows();
		}
		if(citiesJSON) {
			renderCities();
		}
	}
};
//window.onload = init;
window.addEventListener('load', init);