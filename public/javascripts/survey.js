/**
 * @author Massimo De Marchi
 * @created 19/9/15
 *
 * Copyright inkOfPixel 2015.
 */

var Category = React.createClass({displayName: "Category",
	getInitialState: function() {
		return {
			menuVisible: false,
			value: null
		};
	},

	handleDropMenu: function(e) {
		this.setState({menuVisible: !this.state.menuVisible});
		e.preventDefault();
	},

	render: function() {
		var menu = this.props.categories.map(function(category) {
			return (
				React.createElement("button", {className: "category-item", onClick: function(e) {
					e.preventDefault();
					this.setState({
						menuVisible: false,
						value: category
					});
					this.props.onUserInput(true);
				}.bind(this), key: category}, category)
			);
		}.bind(this));

		var menuClass = "menu" + (this.state.menuVisible ? " visible" : "");
		var label = this.state.value ? this.state.value : this.props.placeholder;
		var selectedClass = "selected-category" + (this.state.value ? "" : " not-set");
		selectedClass += (this.state.menuVisible ? " menu-visible" : "");

		return (
			React.createElement("div", {className: "input-category " + this.props.name}, 
				React.createElement("input", {type: "hidden", name: this.props.name, value: label}), 
				React.createElement("button", {className: selectedClass, onClick: this.handleDropMenu}, 
					label, 
					React.createElement("div", {className: "icon"}, 
						React.createElement("svg", {width: "100%", height: "100%", viewBox: "0 0 10 6", preserveRatio: "xMidYmid"}, 
							React.createElement("polygon", {points: "0,0 5,6 10,0"})
						)
					)
				), 
				React.createElement("div", {className: "menu-wrapper"}, 
					React.createElement("div", {className: menuClass}, 
						menu
					)
				)
			)
		);
	}
});
/**
 * @author Massimo De Marchi
 * @created 9/21/15
 *
 * Copyright inkOfPixel 2015.
 */

var InputGrid = React.createClass({displayName: "InputGrid",
	getInitialState: function() {
		return ({
			selected: {}
		});
	},

	itemClicked: function(item) {
		var selectedDeepCopy = JSON.parse(JSON.stringify(this.state.selected));
		var selectedCount = Object.keys(selectedDeepCopy).length;
		var isComplete = selectedCount >= this.props.min;

		if(selectedDeepCopy[item]) {
			delete selectedDeepCopy[item];
			this.setState({selected: selectedDeepCopy}, function() {
				if(isComplete && Object.keys(selectedDeepCopy).length < this.props.min) {
					this.props.onUserInput(false);
				}
			}.bind(this));
		} else if(selectedCount < this.props.max) {
			selectedDeepCopy[item] = true;
			this.setState({selected: selectedDeepCopy}, function() {
				if(!isComplete && Object.keys(selectedDeepCopy).length >= this.props.min) {
					this.props.onUserInput(true);
				}
			});
		}
	},

	getSelection: function() {
		return Object.keys(this.state.selected);
	},

	render: function () {
		var buttons = this.props.itemList.map(function(item) {
			var className = "item" + (this.state.selected[item] ? " selected" : "");
			return (
				React.createElement("button", {key: item, 
				        className: className, onClick: function(e) {
				            e.preventDefault();
				            this.itemClicked(item);
				        }.bind(this)}, 
					item
				)
			);
		}.bind(this));

		return (
			React.createElement("div", {className: "input-grid"}, 
				buttons
			)
		);
	}
});
 
 
/**
 * @author Massimo De Marchi
 * @created 9/21/15
 *
 * Copyright inkOfPixel 2015.
 */

var InputMail = React.createClass({displayName: "InputMail",
	mailValidator: /.+@.+\..+$/i,

	getInitialState: function() {
		return {
			text: null,
			validFormat: false
		}
	},

	getEmail: function() {
		return this.state.validFormat ? this.state.text : null;
	},

	handleOnFocus: function() {
		if(this.state.text == null) {
			this.setState({text: ""});
		}
	},

	handleOnBlur: function() {
		if(this.state.text.length == 0) {
			this.setState({text: null});
		}
	},

	handleUserTyping: function(e) {
		var newText = e.target.value;
		var formatCheck = this.mailValidator.test(newText);
		if(formatCheck != this.state.validFormat) {
			this.props.onUserInput(formatCheck);
			this.setState({text: newText, validFormat: formatCheck});
		} else {
			this.setState({text: newText});
		}
	},

	render: function () {
		var text = this.state.text != null ? text = this.state.text : "jsmith@example.com";
		var fieldClassName = 'field' + (this.state.validFormat ? " valid" : "");
		fieldClassName += this.state.text == null ? " placeholder" : "";

		return (
			React.createElement("div", {className: "input-mail"}, 
				React.createElement("input", {name: this.props.name, 
				       className: fieldClassName, 
				       type: "mail", 
				       value: text, 
				       onChange: this.handleUserTyping, 
				       onFocus: this.handleOnFocus, 
				       onBlur: this.handleOnBlur}
					)
			)
		);
	}
});
 
 
 
/**
 * @author Massimo De Marchi
 * @created 9/21/15
 *
 * Copyright inkOfPixel 2015.
 */

var InputOption = React.createClass({displayName: "InputOption",
	getInitialState: function() {
		return {
			option: null
		}
	},

	getSelectedOption: function() {
		return this.state.option;
	},

	handleSelectOption: function(option) {
		if(this.state.option != option) {
			this.setState({option: option}, function() {
				this.props.onUserInput(true);
			}.bind(this));
		}
	},

	render: function () {
		var optionsButton = this.props.options.map(function(option) {
			var optionClassName = 'option' + (this.state.option == option ? ' selected' : '');
			return (
				React.createElement("button", {className: optionClassName, 
				        key: option, 
				        onClick: function(e) {
				            e.preventDefault();
				            this.handleSelectOption(option);
				        }.bind(this)
					}, 
					option
				)
			);
		}.bind(this));
		return (
			React.createElement("div", {className: "input-option"}, 
				optionsButton
			)
		);
	}
});
 
/**
 * @author Massimo De Marchi
 * @created 19/9/15
 *
 * Copyright inkOfPixel 2015.
 */

var autocompleteService = new google.maps.places.AutocompleteService();

var InputPlace = React.createClass({displayName: "InputPlace",
	getInitialState: function() {
		return {
			value: null,
			predictions: [],
			selected: null,
			placeID: null
		};
	},

	handleTextChange: function(e) {
		var search = e.target.value;

		if(search.length > 0) {
			var request = {
				input: search,
				types: ['(cities)']
			};
			autocompleteService.getPlacePredictions(request, this.handleNewSuggestions);
			this.setState({value: search , selected: null});
		}

		this.props.onUserInput(false);
		this.setState({value: search, selected: null, predictions: []}, function() {
			var placeInput = React.findDOMNode(this.refs.place);
			placeInput.autocomplete = 'no';
		});
	},

	handleNewSuggestions: function(predictions, status) {
		if(status == google.maps.places.PlacesServiceStatus.OK) {
			var newPredictions = predictions.map(function(prediction) {
				return {
					city: prediction.terms[0].value,
					country: prediction.terms[prediction.terms.length -1].value,
					placeID: prediction.place_id
				};
			});
			this.setState({predictions: newPredictions});
		}
	},

	handleOnFocus: function() {
		if(this.state.value == null) {
			this.setState({value: ""});
		}
	},

	handleOnBlur: function() {
		if(this.state.value.length == 0) {
			this.setState({value: null});
		}
	},

	render: function() {
		var predictions = this.state.predictions.map(function(prediction, index) {
			var description = prediction.city + ", " + prediction.country;

			return (
				React.createElement("button", {className: "place-item", 
				        onClick: function(e) {
				            e.preventDefault();
				            this.setState({
				                value: description,
				                selected: index,
				                placeID: prediction.placeID
				            });
				            this.props.onUserInput(true);
				        }.bind(this), 
				        key: prediction.placeID}, 
					description
				)
			);
		}.bind(this));

		var inputText = (this.state.value != null ? this.state.value : "City");
		var predictionsClass = "predictions" + ((predictions.length > 0 && this.state.selected == null) ? " visible" : "");
		var inputFieldClassName = "place-field" + (this.state.value == null ? " placeholder" : "");

		return (
			React.createElement("div", {className: "input-place"}, 
				React.createElement("input", {ref: "place", type: "text", 
				       className: inputFieldClassName, 
				       name: this.props.name, 
				       autoComplete: "off", 
				       onChange: this.handleTextChange, 
				       onFocus: this.handleOnFocus, 
				       onBlur: this.handleOnBlur, 
					value: inputText}), 
				React.createElement("input", {name: this.props.name + "-placeId", type: "hidden", value: this.state.placeID}), 
				React.createElement("div", {className: "predictions-wrapper"}, 
					React.createElement("div", {className: predictionsClass}, 
						predictions
					)
				)
			)
		);
	}
});

/**
 * @author Massimo De Marchi
 * @created 9/20/15
 *
 * Copyright inkOfPixel 2015.
 */

var InputRate = React.createClass({displayName: "InputRate",
	getInitialState: function() {
		return {
			rate: null
		};
	},

	isSet: function() {
		return this.state.rate != null;
	},

	getRating: function() {
		return this.state.rate;
	},

	render: function() {
		var numberOfOptions = this.props.to - this.props.from +1;
		var ratings = [];
		for(var index = 0; index < numberOfOptions; index++) {
			ratings.push(this.props.from + index);
		}

		ratings = ratings.map(function(rating) {
			var buttonClass = "rating" + (this.state.rate == rating ? " selected" : "");
			return (
				React.createElement("button", {key: rating, 
				        className: buttonClass, 
				        onClick: function(e) {
					e.preventDefault();
					this.setState({rate: rating}, function() {
						this.props.onUserInput(true);
					}.bind(this));
				}.bind(this)}, 
					rating
				)
			);
		}.bind(this));

		return (
			React.createElement("div", {className: "input-rate"}, 
				ratings

			)
		);
	}
});

//<input name={this.props.name} type="hidden" value={this.state.rate}/>
/**
 * @author Massimo De Marchi
 * @created 9/21/15
 *
 * Copyright inkOfPixel 2015.
 */

var InputRateGrid = React.createClass({displayName: "InputRateGrid",
	complete: false,

	validateGrid: function() {
		var completeCheck = true;

		var i = 0;
		while(completeCheck && i < this.props.labels.length) {
			completeCheck = this.refs[("rating" + i)].isSet();
			i++;
		}

		if(this.complete != completeCheck) {
			this.complete = completeCheck;
			this.props.onUserInput(completeCheck);
		}
	},

	getFactors: function() {
		var factors = [];
		for (var i = 0; i < this.props.labels.length; i++) {
			factors.push(this.refs[('rating' + i)].getRating());
		}
		return factors;
	},

	render: function () {
		var rows = this.props.labels.map(function(label, index) {
			return (
				React.createElement("div", {className: "rating-wrapper", key: "rating-wrapper" + index}, 
					React.createElement("span", {className: "label"}, label), 
					React.createElement(InputRate, {ref: "rating" + index, 
					           from: this.props.minRating, 
					           to: this.props.maxRating, 
					           onUserInput: this.validateGrid}
					)
				)
			);
		}.bind(this));
		return (
			React.createElement("div", {className: "input-rate-grid"}, 
				rows
			)
		);
	}
});
 
 
/**
 * @author Massimo De Marchi
 * @created 18/9/15
 *
 * Copyright inkOfPixel 2015.
 */

var startupCategories = [
	'Tech / Hardware',
	'SaaS / Software',
	'eCommerce',
	'Health and BioTech',
	'IoT and Virtual Reality',
	'Big Data',
	'FinTech',
	'Consumer mobile/web applications (Games, etc.)',
	'Other'
];
var cities = [
	'Amsterdam', 'Athens', 'Barcelona', 'Berlin', 'Birmingham', 'Budapest', 'Bucharest', 'Copenhagen', 'Dublin',
	'Glasgow', 'Hamburg', 'Helsinki', 'Lisbon', 'London', 'Madrid', 'Malta', 'Manchester', 'Milan', 'Munich',
	'Oslo', 'Paris', 'Prague', 'Riga', 'Rome', 'Stockholm', 'Luxembourg', 'Vienna', 'Tallinn', 'Warsaw', 'Zurich'
];

var factorsLabels = [
	'Access to talent',
	'Access to capital',
	'Monthly costs (burnrate)',
	'Quality of the ecosystem (access to support, partners,..)'
];

var citiesToBeRated = [
	'Copenhagen',
	'Dublin',
	'Manchester',
	'Stockholm',
	'Munich',
	'Milan',
	'Vienna'
];

var ages = [];
for (var i = 15; i < 100; i++) {
	ages.push(i);

}


var Survey = React.createClass({displayName: "Survey",
	numberOfQuestions: 12,

	getInitialState: function() {
		return {
			currentQuestion: 0,
			canGoNext: false,
			askEmail: false,
			thankYou: false
		};
	},

	onNextPage: function(e) {
		if(this.state.canGoNext) {
			if(this.state.currentQuestion == 0) {
				fbq('track', 'Lead');
			}
			if(this.state.askEmail) {
				this.setState({
					askEmail: false,
					thankYou: true,
					canGoNext: false,
					currentQuestion: this.state.currentQuestion +1
				});
			} else if(this.state.currentQuestion + 1 < this.numberOfQuestions) {
				this.setState({
					currentQuestion: this.state.currentQuestion +1,
					canGoNext: false
				});
			} else {
				this.setState({
					askEmail: true,
					currentQuestion: this.state.currentQuestion +1,
					canGoNext: false
				});
			}
		}
		e.preventDefault();
	},

	handleQuestionAnswer: function(valid) {
		this.setState({canGoNext: valid});
	},

	submitData: function(e) {
		e.preventDefault();
		var form = React.findDOMNode(this.refs.survey);
		var formData = new FormData(form);

		formData.append('yourStartupLocationFactors', this.refs.yourStartupLocationFactors.getFactors());
		formData.append('hotSpots', this.refs.citiesGrid.getSelection());
		citiesToBeRated.forEach(function(city) {
			formData.append((city + '-factors'), this.refs[(city + '-factors')].getFactors());
		}.bind(this));
		formData.append('gender', this.refs.gender.getSelectedOption());
		var email = this.refs.email.getEmail();
		if(email) {
			formData.append('email', email);
		}


		var request = new XMLHttpRequest();

		request.addEventListener('load', function(event) {
			console.log(event);
		});

		request.open("POST", "/survey");
		request.send(formData);
		fbq('track', 'CompleteRegistration');

		this.setState({
			canGoNext: false,
			askEmail: false,
			thankYou: true,
			currentQuestion: this.state.currentQuestion +1
		});
	},

	render: function() {
		var currentQuestion = this.state.currentQuestion;
		var buttonNextClassName = "next" + (this.state.canGoNext ? "": " disabled");
		var questions = [];

		// Question 1
		questions.push(
			React.createElement("div", null, 
				React.createElement("h1", null, "Be part of the heat"), 
				React.createElement("div", {className: "content"}, 
					React.createElement("p", null, "Have you founded a Startup in Europe?"), 
					React.createElement("p", null, "And if yes, in which city is your Startup located?"), 
					React.createElement(InputPlace, {name: "startupCity", onUserInput: this.handleQuestionAnswer}), 
					React.createElement("br", null), 
					React.createElement("button", {className: buttonNextClassName, onClick: this.onNextPage}, "next")
				)
			)
		);

		// Question 2
		questions.push(
			React.createElement("div", null, 
				React.createElement("p", null, "Which category best describes your startup?"), 
				React.createElement(Category, {name: "startupCategory", 
				          placeholder: "Select category..", 
				          categories: startupCategories, 
				          onUserInput: this.handleQuestionAnswer}), 
				React.createElement("button", {className: buttonNextClassName, onClick: this.onNextPage}, "next")
			)
		);

		// Question 3
		questions.push(
			React.createElement("div", null, 
				React.createElement("p", null, "Which city are you from?"), 
				React.createElement(InputPlace, {name: "fromCity", onUserInput: this.handleQuestionAnswer}), 
				React.createElement("button", {className: buttonNextClassName, onClick: this.onNextPage}, "next")
			)
		);


		// Question 4
		questions.push(
			React.createElement("div", null, 
				React.createElement("p", null, "How relevant were the following reasons for the location of your Start-Up?"), 
				React.createElement("p", null, "0 meaning not relevant and 5 meaning extremely relevant"), 
				React.createElement(InputRateGrid, {ref: "yourStartupLocationFactors", 
				               minRating: 0, 
				               maxRating: 5, 
				               labels: factorsLabels, 
				               onUserInput: this.handleQuestionAnswer
					}), 
				React.createElement("button", {className: buttonNextClassName, onClick: this.onNextPage}, "next")
			)
		);

		// Question 5
		questions.push(
			React.createElement("div", null, 
				React.createElement("p", null, "In which of the cities below would you consider to start your company if you could choose" + ' ' +
					"freely? Pick up to 5."), 
				React.createElement(InputGrid, {ref: "citiesGrid", min: "1", max: "5", itemList: cities, onUserInput: this.handleQuestionAnswer}), 
				React.createElement("button", {className: buttonNextClassName, onClick: this.onNextPage}, "next")
			)
		);

		// Question 6-12
		citiesToBeRated.forEach(function(city, index) {
			questions.push(
				React.createElement("div", null, 
					React.createElement("p", null, "What is your opinion on the following six cities? Please rate their quality as Start-Up Hub" + ' ' +
						"along the following five criteria. 0 meaning not satisfactory and 5 meaning extremely satisfactory."), 
					React.createElement("h3", {className: "city-name"}, city), 
					React.createElement("p", {className: "city-number"}, "City ", index +1, " of ", citiesToBeRated.length), 
					React.createElement(InputRateGrid, {ref: city + '-factors', 
					               minRating: 0, 
					               maxRating: 5, 
					               labels: factorsLabels, 
					               onUserInput: this.handleQuestionAnswer
						}), 
					React.createElement("button", {className: buttonNextClassName, onClick: this.onNextPage}, "next")
				)
			);
		}.bind(this));

		// Question 13
		questions.push(
			React.createElement("div", null, 
				React.createElement("p", null, "Please indicate your gender (Male or Female)"), 
				React.createElement(InputOption, {ref: "gender", 
				             options: ['M', 'F'], 
				             onUserInput: this.handleQuestionAnswer
					}), 
				React.createElement("button", {className: buttonNextClassName, onClick: this.onNextPage}, "next")
			)
		);

		// Question 14
		questions.push(
			React.createElement("div", null, 
				React.createElement("p", null, "Please indicate your age"), 
				React.createElement(Category, {name: "age", 
				          placeholder: "age", 
				          categories: ages, 
				          onUserInput: this.handleQuestionAnswer}), 
				React.createElement("button", {className: buttonNextClassName, onClick: this.onNextPage}, "next")
			)
		);

		// Question 15
		questions.push(
			React.createElement("div", null, 
				React.createElement("p", null, "Please leave your email address and name if you want to be informed about the results"), 
				React.createElement(InputMail, {ref: "email", 
				           name: "tmp-mail", 
				           onUserInput: this.handleQuestionAnswer
					}), 
				React.createElement("button", {className: "skip", onClick: this.submitData}, "skip"), 
				React.createElement("button", {className: buttonNextClassName, onClick: this.submitData}, "next")
			)
		);


		// Thank you
		questions.push(
			React.createElement("div", null, 
				React.createElement("h3", {className: "thank-you"}, "Thank you!")
			)
		);


		// Decide which one is visible
		questions = questions.map(function(question, index) {
			var questionClass = index == currentQuestion ? "question visible" : "question not-visible";
			return (
				React.createElement("div", {className: questionClass, key: index}, 
					question.props.children
				)
			);
		});

		var pageNumber = null;
		if(this.state.currentQuestion < this.numberOfQuestions) {
			pageNumber = React.createElement("div", {className: "page-number"}, this.state.currentQuestion +1, " / ", this.numberOfQuestions);
		}

		return (
			React.createElement("div", {className: "wrapper"}, 
				React.createElement("form", {action: "/survey", method: "post", ref: "survey", autoComplete: "noo"}, 
					questions, 
					pageNumber
				)
			)
		);
	}
});

React.render(
	React.createElement(Survey, null),
	document.getElementById('survey')
);

