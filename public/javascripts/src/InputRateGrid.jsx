/**
 * @author Massimo De Marchi
 * @created 9/21/15
 *
 * Copyright inkOfPixel 2015.
 */

var InputRateGrid = React.createClass({
	complete: false,

	validateGrid: function() {
		var completeCheck = true;

		var i = 0;
		while(completeCheck && i < this.props.labels.length) {
			completeCheck = this.refs[("rating" + i)].isSet();
			i++;
		}

		if(this.complete != completeCheck) {
			this.complete = completeCheck;
			this.props.onUserInput(completeCheck);
		}
	},

	getFactors: function() {
		var factors = [];
		for (var i = 0; i < this.props.labels.length; i++) {
			factors.push(this.refs[('rating' + i)].getRating());
		}
		return factors;
	},

	render: function () {
		var rows = this.props.labels.map(function(label, index) {
			return (
				<div className="rating-wrapper" key={"rating-wrapper" + index}>
					<span className="label">{label}</span>
					<InputRate ref={"rating" + index}
					           from={this.props.minRating}
					           to={this.props.maxRating}
					           onUserInput={this.validateGrid}>
					</InputRate>
				</div>
			);
		}.bind(this));
		return (
			<div className="input-rate-grid">
				{rows}
			</div>
		);
	}
});
 
 