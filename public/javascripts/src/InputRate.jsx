/**
 * @author Massimo De Marchi
 * @created 9/20/15
 *
 * Copyright inkOfPixel 2015.
 */

var InputRate = React.createClass({
	getInitialState: function() {
		return {
			rate: null
		};
	},

	isSet: function() {
		return this.state.rate != null;
	},

	getRating: function() {
		return this.state.rate;
	},

	render: function() {
		var numberOfOptions = this.props.to - this.props.from +1;
		var ratings = [];
		for(var index = 0; index < numberOfOptions; index++) {
			ratings.push(this.props.from + index);
		}

		ratings = ratings.map(function(rating) {
			var buttonClass = "rating" + (this.state.rate == rating ? " selected" : "");
			return (
				<button key={rating}
				        className={buttonClass}
				        onClick={function(e) {
					e.preventDefault();
					this.setState({rate: rating}, function() {
						this.props.onUserInput(true);
					}.bind(this));
				}.bind(this)}>
					{rating}
				</button>
			);
		}.bind(this));

		return (
			<div className="input-rate">
				{ratings}

			</div>
		);
	}
});

//<input name={this.props.name} type="hidden" value={this.state.rate}/>