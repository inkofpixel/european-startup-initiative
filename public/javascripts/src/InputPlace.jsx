/**
 * @author Massimo De Marchi
 * @created 19/9/15
 *
 * Copyright inkOfPixel 2015.
 */

var autocompleteService = new google.maps.places.AutocompleteService();

var InputPlace = React.createClass({
	getInitialState: function() {
		return {
			value: null,
			predictions: [],
			selected: null,
			placeID: null
		};
	},

	handleTextChange: function(e) {
		var search = e.target.value;

		if(search.length > 0) {
			var request = {
				input: search,
				types: ['(cities)']
			};
			autocompleteService.getPlacePredictions(request, this.handleNewSuggestions);
			this.setState({value: search , selected: null});
		}

		this.props.onUserInput(false);
		this.setState({value: search, selected: null, predictions: []}, function() {
			var placeInput = React.findDOMNode(this.refs.place);
			placeInput.autocomplete = 'no';
		});
	},

	handleNewSuggestions: function(predictions, status) {
		if(status == google.maps.places.PlacesServiceStatus.OK) {
			var newPredictions = predictions.map(function(prediction) {
				return {
					city: prediction.terms[0].value,
					country: prediction.terms[prediction.terms.length -1].value,
					placeID: prediction.place_id
				};
			});
			this.setState({predictions: newPredictions});
		}
	},

	handleOnFocus: function() {
		if(this.state.value == null) {
			this.setState({value: ""});
		}
	},

	handleOnBlur: function() {
		if(this.state.value.length == 0) {
			this.setState({value: null});
		}
	},

	render: function() {
		var predictions = this.state.predictions.map(function(prediction, index) {
			var description = prediction.city + ", " + prediction.country;

			return (
				<button className="place-item"
				        onClick={function(e) {
				            e.preventDefault();
				            this.setState({
				                value: description,
				                selected: index,
				                placeID: prediction.placeID
				            });
				            this.props.onUserInput(true);
				        }.bind(this)}
				        key={prediction.placeID}>
					{description}
				</button>
			);
		}.bind(this));

		var inputText = (this.state.value != null ? this.state.value : "City");
		var predictionsClass = "predictions" + ((predictions.length > 0 && this.state.selected == null) ? " visible" : "");
		var inputFieldClassName = "place-field" + (this.state.value == null ? " placeholder" : "");

		return (
			<div className="input-place">
				<input ref="place" type="text"
				       className={inputFieldClassName}
				       name={this.props.name}
				       autoComplete="off"
				       onChange={this.handleTextChange}
				       onFocus={this.handleOnFocus}
				       onBlur={this.handleOnBlur}
					value={inputText}/>
				<input name={this.props.name + "-placeId"} type="hidden" value={this.state.placeID}/>
				<div className="predictions-wrapper">
					<div className={predictionsClass}>
						{predictions}
					</div>
				</div>
			</div>
		);
	}
});
