/**
 * @author Massimo De Marchi
 * @created 9/21/15
 *
 * Copyright inkOfPixel 2015.
 */

var InputMail = React.createClass({
	mailValidator: /.+@.+\..+$/i,

	getInitialState: function() {
		return {
			text: null,
			validFormat: false
		}
	},

	getEmail: function() {
		return this.state.validFormat ? this.state.text : null;
	},

	handleOnFocus: function() {
		if(this.state.text == null) {
			this.setState({text: ""});
		}
	},

	handleOnBlur: function() {
		if(this.state.text.length == 0) {
			this.setState({text: null});
		}
	},

	handleUserTyping: function(e) {
		var newText = e.target.value;
		var formatCheck = this.mailValidator.test(newText);
		if(formatCheck != this.state.validFormat) {
			this.props.onUserInput(formatCheck);
			this.setState({text: newText, validFormat: formatCheck});
		} else {
			this.setState({text: newText});
		}
	},

	render: function () {
		var text = this.state.text != null ? text = this.state.text : "jsmith@example.com";
		var fieldClassName = 'field' + (this.state.validFormat ? " valid" : "");
		fieldClassName += this.state.text == null ? " placeholder" : "";

		return (
			<div className="input-mail">
				<input name={this.props.name}
				       className={fieldClassName}
				       type="mail"
				       value={text}
				       onChange={this.handleUserTyping}
				       onFocus={this.handleOnFocus}
				       onBlur={this.handleOnBlur}
					/>
			</div>
		);
	}
});
 
 
 