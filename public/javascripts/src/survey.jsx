/**
 * @author Massimo De Marchi
 * @created 18/9/15
 *
 * Copyright inkOfPixel 2015.
 */

var startupCategories = [
	'Tech / Hardware',
	'SaaS / Software',
	'eCommerce',
	'Health and BioTech',
	'IoT and Virtual Reality',
	'Big Data',
	'FinTech',
	'Consumer mobile/web applications (Games, etc.)',
	'Other'
];
var cities = [
	'Amsterdam', 'Athens', 'Barcelona', 'Berlin', 'Birmingham', 'Budapest', 'Bucharest', 'Copenhagen', 'Dublin',
	'Glasgow', 'Hamburg', 'Helsinki', 'Lisbon', 'London', 'Madrid', 'Malta', 'Manchester', 'Milan', 'Munich',
	'Oslo', 'Paris', 'Prague', 'Riga', 'Rome', 'Stockholm', 'Luxembourg', 'Vienna', 'Tallinn', 'Warsaw', 'Zurich'
];

var factorsLabels = [
	'Access to talent',
	'Access to capital',
	'Monthly costs (burnrate)',
	'Quality of the ecosystem (access to support, partners,..)'
];

var citiesToBeRated = [
	'Copenhagen',
	'Dublin',
	'Manchester',
	'Stockholm',
	'Munich',
	'Milan',
	'Vienna'
];

var ages = [];
for (var i = 15; i < 100; i++) {
	ages.push(i);

}


var Survey = React.createClass({
	numberOfQuestions: 12,

	getInitialState: function() {
		return {
			currentQuestion: 0,
			canGoNext: false,
			askEmail: false,
			thankYou: false
		};
	},

	onNextPage: function(e) {
		if(this.state.canGoNext) {
			if(this.state.currentQuestion == 0) {
				fbq('track', 'Lead');
			}
			if(this.state.askEmail) {
				this.setState({
					askEmail: false,
					thankYou: true,
					canGoNext: false,
					currentQuestion: this.state.currentQuestion +1
				});
			} else if(this.state.currentQuestion + 1 < this.numberOfQuestions) {
				this.setState({
					currentQuestion: this.state.currentQuestion +1,
					canGoNext: false
				});
			} else {
				this.setState({
					askEmail: true,
					currentQuestion: this.state.currentQuestion +1,
					canGoNext: false
				});
			}
		}
		e.preventDefault();
	},

	handleQuestionAnswer: function(valid) {
		this.setState({canGoNext: valid});
	},

	submitData: function(e) {
		e.preventDefault();
		var form = React.findDOMNode(this.refs.survey);
		var formData = new FormData(form);

		formData.append('yourStartupLocationFactors', this.refs.yourStartupLocationFactors.getFactors());
		formData.append('hotSpots', this.refs.citiesGrid.getSelection());
		citiesToBeRated.forEach(function(city) {
			formData.append((city + '-factors'), this.refs[(city + '-factors')].getFactors());
		}.bind(this));
		formData.append('gender', this.refs.gender.getSelectedOption());
		var email = this.refs.email.getEmail();
		if(email) {
			formData.append('email', email);
		}


		var request = new XMLHttpRequest();

		request.addEventListener('load', function(event) {
			console.log(event);
		});

		request.open("POST", "/survey");
		request.send(formData);
		fbq('track', 'CompleteRegistration');

		this.setState({
			canGoNext: false,
			askEmail: false,
			thankYou: true,
			currentQuestion: this.state.currentQuestion +1
		});
	},

	render: function() {
		var currentQuestion = this.state.currentQuestion;
		var buttonNextClassName = "next" + (this.state.canGoNext ? "": " disabled");
		var questions = [];

		// Question 1
		questions.push(
			<div>
				<h1>Be part of the heat</h1>
				<div className="content">
					<p>Have you founded a Startup in Europe?</p>
					<p>And if yes, in which city is your Startup located?</p>
					<InputPlace name="startupCity" onUserInput={this.handleQuestionAnswer}></InputPlace>
					<br/>
					<button className={buttonNextClassName} onClick={this.onNextPage}>next</button>
				</div>
			</div>
		);

		// Question 2
		questions.push(
			<div>
				<p>Which category best describes your startup?</p>
				<Category name="startupCategory"
				          placeholder="Select category.."
				          categories={startupCategories}
				          onUserInput={this.handleQuestionAnswer}></Category>
				<button className={buttonNextClassName} onClick={this.onNextPage}>next</button>
			</div>
		);

		// Question 3
		questions.push(
			<div>
				<p>Which city are you from?</p>
				<InputPlace name="fromCity" onUserInput={this.handleQuestionAnswer}></InputPlace>
				<button className={buttonNextClassName} onClick={this.onNextPage}>next</button>
			</div>
		);


		// Question 4
		questions.push(
			<div>
				<p>How relevant were the following reasons for the location of your Start-Up?</p>
				<p>0 meaning not relevant and 5 meaning extremely relevant</p>
				<InputRateGrid ref="yourStartupLocationFactors"
				               minRating={0}
				               maxRating={5}
				               labels={factorsLabels}
				               onUserInput={this.handleQuestionAnswer}
					></InputRateGrid>
				<button className={buttonNextClassName} onClick={this.onNextPage}>next</button>
			</div>
		);

		// Question 5
		questions.push(
			<div>
				<p>In which of the cities below would you consider to start your company if you could choose
					freely? Pick up to 5.</p>
				<InputGrid ref="citiesGrid" min="1" max="5" itemList={cities} onUserInput={this.handleQuestionAnswer}></InputGrid>
				<button className={buttonNextClassName} onClick={this.onNextPage}>next</button>
			</div>
		);

		// Question 6-12
		citiesToBeRated.forEach(function(city, index) {
			questions.push(
				<div>
					<p>What is your opinion on the following six cities? Please rate their quality as Start-Up Hub
						along the following five criteria. 0 meaning not satisfactory and 5 meaning extremely satisfactory.</p>
					<h3 className="city-name">{city}</h3>
					<p className="city-number">City {index +1} of {citiesToBeRated.length}</p>
					<InputRateGrid ref={city + '-factors'}
					               minRating={0}
					               maxRating={5}
					               labels={factorsLabels}
					               onUserInput={this.handleQuestionAnswer}
						></InputRateGrid>
					<button className={buttonNextClassName} onClick={this.onNextPage}>next</button>
				</div>
			);
		}.bind(this));

		// Question 13
		questions.push(
			<div>
				<p>Please indicate your gender (Male or Female)</p>
				<InputOption ref="gender"
				             options={['M', 'F']}
				             onUserInput={this.handleQuestionAnswer}
					></InputOption>
				<button className={buttonNextClassName} onClick={this.onNextPage}>next</button>
			</div>
		);

		// Question 14
		questions.push(
			<div>
				<p>Please indicate your age</p>
				<Category name="age"
				          placeholder="age"
				          categories={ages}
				          onUserInput={this.handleQuestionAnswer}></Category>
				<button className={buttonNextClassName} onClick={this.onNextPage}>next</button>
			</div>
		);

		// Question 15
		questions.push(
			<div>
				<p>Please leave your email address and name if you want to be informed about the results</p>
				<InputMail ref="email"
				           name="tmp-mail"
				           onUserInput={this.handleQuestionAnswer}
					></InputMail>
				<button className="skip" onClick={this.submitData}>skip</button>
				<button className={buttonNextClassName} onClick={this.submitData}>next</button>
			</div>
		);


		// Thank you
		questions.push(
			<div>
				<h3 className="thank-you">Thank you!</h3>
			</div>
		);


		// Decide which one is visible
		questions = questions.map(function(question, index) {
			var questionClass = index == currentQuestion ? "question visible" : "question not-visible";
			return (
				<div className={questionClass} key={index}>
					{question.props.children}
				</div>
			);
		});

		var pageNumber = null;
		if(this.state.currentQuestion < this.numberOfQuestions) {
			pageNumber = <div className="page-number">{this.state.currentQuestion +1} / {this.numberOfQuestions}</div>;
		}

		return (
			<div className="wrapper">
				<form action="/survey" method="post" ref="survey" autoComplete={"noo"}>
					{questions}
					{pageNumber}
				</form>
			</div>
		);
	}
});

React.render(
	<Survey />,
	document.getElementById('survey')
);

