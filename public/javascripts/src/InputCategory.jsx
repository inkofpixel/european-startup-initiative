/**
 * @author Massimo De Marchi
 * @created 19/9/15
 *
 * Copyright inkOfPixel 2015.
 */

var Category = React.createClass({
	getInitialState: function() {
		return {
			menuVisible: false,
			value: null
		};
	},

	handleDropMenu: function(e) {
		this.setState({menuVisible: !this.state.menuVisible});
		e.preventDefault();
	},

	render: function() {
		var menu = this.props.categories.map(function(category) {
			return (
				<button className="category-item" onClick={function(e) {
					e.preventDefault();
					this.setState({
						menuVisible: false,
						value: category
					});
					this.props.onUserInput(true);
				}.bind(this)} key={category}>{category}</button>
			);
		}.bind(this));

		var menuClass = "menu" + (this.state.menuVisible ? " visible" : "");
		var label = this.state.value ? this.state.value : this.props.placeholder;
		var selectedClass = "selected-category" + (this.state.value ? "" : " not-set");
		selectedClass += (this.state.menuVisible ? " menu-visible" : "");

		return (
			<div className={"input-category " + this.props.name}>
				<input type="hidden" name={this.props.name} value={label}/>
				<button className={selectedClass} onClick={this.handleDropMenu}>
					{label}
					<div className="icon">
						<svg width="100%" height="100%" viewBox="0 0 10 6" preserveRatio="xMidYmid">
							<polygon points="0,0 5,6 10,0" />
						</svg>
					</div>
				</button>
				<div className="menu-wrapper">
					<div className={menuClass}>
						{menu}
					</div>
				</div>
			</div>
		);
	}
});