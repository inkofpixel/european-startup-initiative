/**
 * @author Massimo De Marchi
 * @created 9/21/15
 *
 * Copyright inkOfPixel 2015.
 */

var InputGrid = React.createClass({
	getInitialState: function() {
		return ({
			selected: {}
		});
	},

	itemClicked: function(item) {
		var selectedDeepCopy = JSON.parse(JSON.stringify(this.state.selected));
		var selectedCount = Object.keys(selectedDeepCopy).length;
		var isComplete = selectedCount >= this.props.min;

		if(selectedDeepCopy[item]) {
			delete selectedDeepCopy[item];
			this.setState({selected: selectedDeepCopy}, function() {
				if(isComplete && Object.keys(selectedDeepCopy).length < this.props.min) {
					this.props.onUserInput(false);
				}
			}.bind(this));
		} else if(selectedCount < this.props.max) {
			selectedDeepCopy[item] = true;
			this.setState({selected: selectedDeepCopy}, function() {
				if(!isComplete && Object.keys(selectedDeepCopy).length >= this.props.min) {
					this.props.onUserInput(true);
				}
			});
		}
	},

	getSelection: function() {
		return Object.keys(this.state.selected);
	},

	render: function () {
		var buttons = this.props.itemList.map(function(item) {
			var className = "item" + (this.state.selected[item] ? " selected" : "");
			return (
				<button key={item}
				        className={className} onClick={function(e) {
				            e.preventDefault();
				            this.itemClicked(item);
				        }.bind(this)}>
					{item}
				</button>
			);
		}.bind(this));

		return (
			<div className="input-grid">
				{buttons}
			</div>
		);
	}
});
 
 