/**
 * @author Massimo De Marchi
 * @created 9/21/15
 *
 * Copyright inkOfPixel 2015.
 */

var InputOption = React.createClass({
	getInitialState: function() {
		return {
			option: null
		}
	},

	getSelectedOption: function() {
		return this.state.option;
	},

	handleSelectOption: function(option) {
		if(this.state.option != option) {
			this.setState({option: option}, function() {
				this.props.onUserInput(true);
			}.bind(this));
		}
	},

	render: function () {
		var optionsButton = this.props.options.map(function(option) {
			var optionClassName = 'option' + (this.state.option == option ? ' selected' : '');
			return (
				<button className={optionClassName}
				        key={option}
				        onClick={function(e) {
				            e.preventDefault();
				            this.handleSelectOption(option);
				        }.bind(this)}
					>
					{option}
				</button>
			);
		}.bind(this));
		return (
			<div className="input-option">
				{optionsButton}
			</div>
		);
	}
});
 