/**
 * Created by MM on 18/09/15.
 */
var navBar;
var speed;
var acceleration;
var firstSectionSelector;

function init() {
	var maxDistance = document.body.offsetHeight;

	var maxTime = 4000;
	speed = maxDistance / maxTime;

	var maxAcceleration = 2;
	var a = maxAcceleration / (maxDistance * maxDistance);
	acceleration = function(distance) {
		return a * distance * distance;
	};

	navBar = d3.select(".navigation-bar");
	firstSectionSelector = d3.select(".navigation-bar li:first-child .anchor").attr('href');
	firstSectionSelector = firstSectionSelector.substring(firstSectionSelector.indexOf("#"));

	d3.selectAll(".navigation-bar .anchor").on('click', function() {
		var targetSectionSelector = d3.select(this).attr('href');
		targetSectionSelector = targetSectionSelector.substring(targetSectionSelector.indexOf("#"));

		scrollToSection(targetSectionSelector);

		d3.event.stopPropagation();
	});

	d3.selectAll(".call-to-action").on('click', function() {
		scrollToSection('#survey');
		d3.event.stopPropagation();
	});

	//if(window.location.hash.length > 0) {
	//	scrollToSection(window.location.hash);
	//}

	window.addEventListener('scroll', changeMenuAppearence);
}

function scrollToSection(sectionSelector) {
	var targetSection = d3.select(sectionSelector);

	var offset = targetSection.node().offsetTop - navBar.node().offsetHeight;
	offset = Math.max(offset, 0);

	var distance = Math.abs(window.pageYOffset - offset);

	d3.transition()
		.duration(distance / (speed + acceleration(distance)))
		.tween("scroll", scrollTween(window.pageYOffset, offset));
}

function changeMenuAppearence() {
    var dy = window.scrollY;
    var firstSection = d3.select(firstSectionSelector);

    navBar.classed("light", dy > (firstSection.node().offsetHeight - navBar.node().offsetHeight));
};

function scrollTween(start, offset) {
    return function() {
        var i = d3.interpolateNumber(start, offset);
        return function(t) { scrollTo(0, i(t)); };
    };
}

window.addEventListener('load', init);