var gulp = require('gulp');
var concat = require('gulp-concat');
var react = require('gulp-react');

gulp.task('compileSurvey', function() {
	console.log("Some JS changed.. I don't miss anything!");
	return gulp.src('./public/javascripts/src/*.jsx')
		.pipe(concat('survey.js'))
		.pipe(react())
		.pipe(gulp.dest('./public/javascripts/'));
});

gulp.task('mergeCSS', function() {
	console.log("Some CSS changed.. I don't miss anything!");
	return gulp.src('./public/stylesheets/src/**')
		.pipe(concat('style.css'))
		.pipe(gulp.dest('./public/stylesheets/bundle'));
});

gulp.task('watchJS', function() {
	console.log("Watching JS");
	return gulp.watch('./public/javascripts/src/*.jsx', ['compileSurvey']);
});

gulp.task('watchCSS', function() {
	console.log("Watching CSS");
	return gulp.watch('./public/stylesheets/src/*.css', ['mergeCSS']);
});

gulp.task('default', ['watchJS', 'watchCSS']);
