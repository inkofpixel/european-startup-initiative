var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Survey = require("../models/Survey.js");
var HotSpot = require("../models/HotSpot.js");

/* GET flows listing. */
router.get('/flows', function(req, res, next) {
	var flows = [];
	Survey.find({}).exec(function(err, surveys) {
		if (err) return next(err);
		surveys.forEach(function(survey) {
			if(survey.startup &&
				survey.startup.location &&
				typeof(survey.startup.location.lat) == 'number' &&
				typeof(survey.startup.location.lng) == 'number' &&
				survey.hotSpots &&
				survey.hotSpots.length > 0) {
				survey.hotSpots.forEach(function(hotSpot) {
					var coordinates = hotspots[hotSpot];
					if(coordinates) {
						flows.push([
							{lat: survey.startup.location.lat, lng: survey.startup.location.lng},
							{lat: coordinates.lat, lng: coordinates.lng}
						]);
					}
				});
			}

		});
		res.json(flows);
	});
});

router.get('/surveys/count', function(req, res, next) {
	Survey.find(function(err, surveys) {
		if (err) return next(err);
		res.json(surveys.length);
	});
});

router.get('/surveys', function(req, res, next) {
	Survey.find(function(err, surveys) {
		if (err) return next(err);
		res.json(surveys);
	});
});

router.get('/hotspots', function(req, res, next) {
	HotSpot.find({}).exec(function(err, hotspots) {
		if (err) return next(err);
		res.json(hotspots);
	});
});

router.get('/hotspots/:cityname', function(req, res, next) {
	HotSpot.find({name: req.params.cityname}, function(err, hotspots) {
		if (err) return next(err);
		res.json(hotspots);
	});
});

//// TODO debug api
//router.get('/delete', function(req, res, next) {
//	Survey.remove({}, function(err) {
//		if (err) return next(err);
//
//		HotSpot
//			.find({ })
//			.exec(function(err, result) {
//				if (err) {return next(err);}
//				result.forEach(function(hotSpot) {
//					hotSpot.prefs = 0;
//					hotSpot.save(function (err) {
//						if (err) return next(err);
//					});
//				});
//			});
//
//		res.redirect('/');
//	});
//});

//// TODO: debug API
//router.get('/randomfill/:number', function(req, res, next) {
//	var randomSurveys = [];
//	var number = req.params.number ? parseInt(req.params.number) || 1 : 1;
//	for (var i = 0; i < number; i++) {
//		randomSurveys.push(getRandomData());
//	}
//	Survey.collection.insert(randomSurveys, function(err, surveys) {
//		if (err) {return next(err);}
//
//		var hotspots = {};
//		randomSurveys.forEach(function(survey) {
//			survey.hotSpots.forEach(function(cityName) {
//				if(hotspots[cityName]) {
//					hotspots[cityName] += 1;
//				} else {
//					hotspots[cityName] = 1;
//				}
//			});
//		});
//
//		HotSpot.
//			find({ }).
//			where('name').in(Object.keys(hotspots)).
//			exec(function(err, result) {
//				if (err) {return next(err);}
//				result.forEach(function(hotSpot) {
//					hotSpot.prefs += hotspots[hotSpot.name];
//					hotSpot.save(function (err) {
//						if (err) return next(err);
//					});
//				});
//			});
//
//		res.redirect('/');
//	});
//});

//// TODO: debug
//router.get('/initdb', function(req, res, next) {
//	var cities = Object.keys(hotspots).map(function(name) {
//		return {
//			name: name,
//			lat: hotspots[name].lat,
//			lng: hotspots[name].lng,
//			prefs: 0
//		};
//	});
//
//	// Remove data
//	HotSpot.remove({}, function(err) {
//		if (err) return next(err);
//		// Store it
//		HotSpot.collection.insert(cities, function(err, myDocuments) {
//			if (err) {
//				return next(err);
//			}
//			res.redirect('/');
//		});
//	});
//});

//router.get('/fixw', function(req, res, next) {
//	HotSpot
//		.find({name: 'Warsaw' })
//		//.where('name').in(formattedData.hotSpots)
//		.exec(function(err, result) {
//			if (err) {return next(err);}
//			result.forEach(function(hotSpot) {
//				hotSpot.lat = 52.233;
//				hotSpot.save(function (err) {
//					if (err) return next(err);
//					res.redirect('/');
//				});
//			});
//		});
//});


function getRandomData() {
	var cities = Object.keys(hotspots);
	return ({
		gender: 'M',
		email: null,
		age: 25,
		placeOfOrigin: {
			description: null,
			placeId: null
		},
		startup: {
			location: {
				description: null,
				placeId: null,
				lat: getRandomInRange(38, 58, 3),
				lng: getRandomInRange(-8, 42, 3)
			},
			category: "A category",
			factors: {
				accessToTalent: 1,
				accessToCapital: 2,
				burnrate: 3,
				ecosystem: 4
			}
		},
		hotSpots: [
			cities[Math.round(Math.random() * (cities.length -1))],
			cities[Math.round(Math.random() * (cities.length -1))],
			(Math.round(Math.random()) ? 'Milan' : 'Barcelona'),
			'London'
			//cities[Math.round(Math.random() * (cities.length -1))],
			//cities[Math.round(Math.random() * (cities.length -1))]
		],
		citiesFactors: {
			Copenhagen: {
				accessToTalent: 1,
				accessToCapital: 2,
				burnrate: 3,
				ecosystem: 4
			},
			Dublin: {
				accessToTalent: 1,
				accessToCapital: 2,
				burnrate: 3,
				ecosystem: 4
			},
			Manchester: {
				accessToTalent: 1,
				accessToCapital: 2,
				burnrate: 3,
				ecosystem: 4
			},
			Stockholm: {
				accessToTalent: 1,
				accessToCapital: 2,
				burnrate: 3,
				ecosystem: 4,
				proximity: 5,
				ease: 6
			},
			Munich: {
				accessToTalent: 1,
				accessToCapital: 2,
				burnrate: 3,
				ecosystem: 4
			},
			Milan: {
				accessToTalent: 1,
				accessToCapital: 2,
				burnrate: 3,
				ecosystem: 4
			},
			Vienna: {
				accessToTalent: 1,
				accessToCapital: 2,
				burnrate: 3,
				ecosystem: 4
			}
		}
	});
}

function getRandomInRange(from, to, fixed) {
	return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
	// .toFixed() returns string, so ' * 1' is a trick to convert to number
}

var hotspots = {
	"Amsterdam": {
		"lat": 52.335,
		"lng": 4.904
	},
	"Athens": {
		"lat": 37.966,
		"lng": 23.716
	},
	"Barcelona": {
		"lat": 41.383,
		"lng": 2.183
	},
	"Berlin": {
		"lat": 52.516,
		"lng": 13.383
	},
	"Birmingham": {
		"lat": 52.483,
		"lng": -1.893
	},
	"Budapest": {
		"lat": 47.492,
		"lng": 19.051
	},
	"Bucharest": {
		"lat": 44.426,
		"lng": 26.102
	},
	"Copenhagen": {
		"lat": 55.676,
		"lng": 12.568
	},
	"Dublin": {
		"lat": 53.347,
		"lng": -6.259
	},
	"Glasgow": {
		"lat": 55.858,
		"lng": -4.259
	},
	"Hamburg": {
		"lat": 53.565,
		"lng": 10.001
	},
	"Helsinki": {
		"lat": 60.170,
		"lng": 24.937
	},
	"Lisbon": {
		"lat": 38.713,
		"lng": -9.139
	},
	"London": {
		"lat": 51.507,
		"lng": 0.127
	},
	"Luxembourg": {
		"lat": 49.600,
		"lng": 6.116
	},
	"Madrid": {
		"lat": 40.400,
		"lng": -3.716
	},
	"Malta": {
		"lat": 35.883,
		"lng": 14.500
	},
	"Manchester": {
		"lat": 53.466,
		"lng": -2.233
	},
	"Milan": {
		"lat": 45.466,
		"lng": 9.183
	},
	"Munich": {
		"lat": 48.133,
		"lng": 11.566
	},
	"Oslo": {
		"lat": 59.950,
		"lng": 10.750
	},
	"Paris": {
		"lat": 48.856,
		"lng": 2.350
	},
	"Prague": {
		"lat": 50.083,
		"lng": 14.416
	},
	"Riga": {
		"lat": 56.948,
		"lng": 24.106
	},
	"Rome": {
		"lat": 41.900,
		"lng": 12.500
	},
	"Stockholm": {
		"lat": 59.329,
		"lng": 18.068
	},
	"Tallinn": {
		"lat": 59.437,
		"lng": 24.745
	},
	"Vienna": {
		"lat": 48.200,
		"lng": 16.366
	},
	"Warsaw": {
		"lat": 52.233,
		"lng": 21.016
	},
	"Zurich": {
		"lat": 47.366,
		"lng": 8.550
	}
};

module.exports = router;





























