var express = require('express');
var http = require("http");
var https = require("https");

var router = express.Router();

var multiparty = require('multiparty');

var mongoose = require('mongoose');
var Survey = require("../models/Survey.js");
var HotSpot = require("../models/HotSpot.js");


/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: "European Startup Initiative" });
});

router.post('/survey', function(req, res, next) {
	// LOG Client IP
	//var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	//console.log('Client IP:', ip);

	var form = new multiparty.Form();

	form.parse(req, function(err, fields, files) {
		formatData(fields, function(formattedData) {

			if(formattedData) {
				Survey.create(formattedData, function(err, data) {
					if (err) return next(err);

					HotSpot
						.find({ })
						.where('name').in(formattedData.hotSpots)
						.exec(function(err, result) {
							if (err) {return next(err);}
							result.forEach(function(hotSpot) {
								hotSpot.prefs += 1;
								hotSpot.save(function (err) {
									if (err) return next(err);
								});
							});
						});

					res.redirect('/');
				});
			} else {
				console.log("INVALID DATA");
				res.redirect('/');
			}
		});
	});
});


function makeHttpRequest(options, onResult) {
	console.log("making HTTP request..");

	var prot = options.port == 443 ? https : http;
	var req = prot.request(options, function(res)
	{
		var output = '';
		console.log(options.host + ':' + res.statusCode);
		res.setEncoding('utf8');

		res.on('data', function (chunk) {
			output += chunk;
		});

		res.on('end', function() {
			var obj = JSON.parse(output);
			onResult(res.statusCode, obj);
		});
	});

	req.on('error', function(err) {
		//res.send('error: ' + err.message);
		console.log(err.message);
	});

	req.end();
}

function getPlaceDetails(placeId, callback) {
	var apiKey = 'AIzaSyAcq5mC2mm3fuhdj6SH1yc8YqWxZmrPJDs';//'AIzaSyAcq5mC2mm3fuhdj6SH1yc8YqWxZmrPJDs';
	var path = '/maps/api/place/details/json?placeid=' + placeId + "&key=" + apiKey;

	var options = {
		host: 'maps.googleapis.com',
		port: 443,
		path: path,
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	};
	makeHttpRequest(options, function(statusCode, result) {
		console.log(statusCode);
		callback(result['result']['geometry']['location']);
	});
}

function formatData(fields, callback) {
	var data = {};
	data.gender =  fields.gender ? fields.gender[0].substring(0, 1) : null;
	data.email = fields.email && fields.email[0] ? fields.email[0].substring(0, 200) : null;
	data.age =  fields.age && fields.age[0] ? parseInt(fields.age[0]) : null;
	data.hotSpots = fields.hotSpots && fields.hotSpots[0] ?
		fields.hotSpots[0].split(',').slice(0,5).map(function(t) {return t.substring(0,100)}) : null;

	data.citiesFactors = {};

	var factors = fields['Copenhagen-factors'] && fields['Copenhagen-factors'][0] ?
		fields['Copenhagen-factors'][0]
			.split(',')
			.slice(0,5)
			.map(function(n) {return parseInt(n)}) : [null, null, null, null, null, null];
	data.citiesFactors.Copenhagen = {
		accessToTalent: factors[0],
		accessToCapital: factors[1],
		burnrate: factors[2],
		ecosystem: factors[3]
	};
	factors = fields['Dublin-factors'] && fields['Dublin-factors'][0] ?
		fields['Dublin-factors'][0]
			.split(',')
			.slice(0,5)
			.map(function(n) {return parseInt(n)}) : [null, null, null, null, null, null];
	data.citiesFactors.Dublin = {
		accessToTalent: factors[0],
		accessToCapital: factors[1],
		burnrate: factors[2],
		ecosystem: factors[3]
	};
	factors = fields['Manchester-factors'] && fields['Manchester-factors'][0] ?
		fields['Manchester-factors'][0]
			.split(',')
			.slice(0,5)
			.map(function(n) {return parseInt(n)}) : [null, null, null, null, null, null];
	data.citiesFactors.Manchester = {
		accessToTalent: factors[0],
		accessToCapital: factors[1],
		burnrate: factors[2],
		ecosystem: factors[3]
	};
	factors = fields['Stockholm-factors'] && fields['Stockholm-factors'][0] ?
		fields['Stockholm-factors'][0]
			.split(',')
			.slice(0,5)
			.map(function(n) {return parseInt(n)}) : [null, null, null, null, null, null];
	data.citiesFactors.Stockholm = {
		accessToTalent: factors[0],
		accessToCapital: factors[1],
		burnrate: factors[2],
		ecosystem: factors[3]
	};
	factors = fields['Munich-factors'] && fields['Munich-factors'][0] ?
		fields['Munich-factors'][0]
			.split(',')
			.slice(0,5)
			.map(function(n) {return parseInt(n)}) : [null, null, null, null, null, null];
	data.citiesFactors.Munich = {
		accessToTalent: factors[0],
		accessToCapital: factors[1],
		burnrate: factors[2],
		ecosystem: factors[3]
	};
	factors = fields['Milan-factors'] && fields['Milan-factors'][0] ?
		fields['Milan-factors'][0]
			.split(',')
			.slice(0,5)
			.map(function(n) {return parseInt(n)}) : [null, null, null, null, null, null];
	data.citiesFactors.Milan = {
		accessToTalent: factors[0],
		accessToCapital: factors[1],
		burnrate: factors[2],
		ecosystem: factors[3]
	};
	factors = fields['Vienna-factors'] && fields['Vienna-factors'][0] ?
		fields['Vienna-factors'][0]
			.split(',')
			.slice(0,5)
			.map(function(n) {return parseInt(n)}) : [null, null, null, null, null, null];
	data.citiesFactors.Vienna = {
		accessToTalent: factors[0],
		accessToCapital: factors[1],
		burnrate: factors[2],
		ecosystem: factors[3]
	};

	data.placeOfOrigin = {
		description: fields['fromCity'] && fields['fromCity'][0] ?
			fields['fromCity'][0] : null,
		placeId: fields['fromCity-placeId'] && fields['fromCity-placeId'][0] ?
			fields['fromCity-placeId'][0] : null
	};

	if(fields['startupCity-placeId'] && fields['startupCity-placeId'][0]) {
		getPlaceDetails(fields['startupCity-placeId'][0], function(result) {
			factors = fields['yourStartupLocationFactors'] && fields['yourStartupLocationFactors'][0] ?
				fields['yourStartupLocationFactors'][0]
					.split(',')
					.slice(0, 5)
					.map(function(n) {return parseInt(n)}) : [null, null, null, null, null, null];
			data.startup = {
				location: {
					description: fields['startupCity'] && fields['startupCity'][0] ?
						fields['startupCity'][0] : null,
					placeId: fields['startupCity-placeId'] && fields['startupCity-placeId'][0] ?
						fields['startupCity-placeId'][0] : null,
					lat: result.lat,
					lng: result.lng
				},
				category: fields['startupCategory'] && fields['startupCategory'][0] ?
					fields['startupCategory'][0] : null,
				factors: {
					accessToTalent: factors[0],
					accessToCapital: factors[1],
					burnrate: factors[2],
					ecosystem: factors[3]
				}
			};

			callback(data);
		});
	} else {
		callback(null);
	}
}


module.exports = router;
