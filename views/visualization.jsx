var React = require('react');

var VisualizationSection = React.createClass({

	render: function() {
		return (
			<main id="heat-map">
				<div className="description">
					<h1 className="title">Where is Europe's <br/> next Silicon Valley?</h1>
					<div className="heatmap-description">
						<p>Mapping the perceived quality of startup hubs in Europe,</p>
						<p>this shows where startup founders in Europe are from </p>
						<p>and which places they consider best to startup.</p>
					</div>
					<a className="call-to-action" href="#survey">Be part of the heat</a>

				</div>

				<a href="http://www.inkofpixel.com"><div id="watermark"></div></a>

				<div id="legend">
					<div className="legend-line"></div>
					<div className="legend-start-label">startup founders are from</div>
					<div className="legend-end-label">best places to startup</div>
				</div>
				<svg id="visualization">
					<defs>
						<svg id="flowsvg" viewBox="0 0 102 4" preserveAspectRatio="none">
							<g>
								<path d="M0,2c0,0,1.6-0.2,4.3-0.1c2.7,0,6.6-0.1,11.3-0.1C25,1.8,37.5,1.7,50,1.7c6.2,0,12.5-0.1,18.4-0.1
        c5.9-0.1,11.3-0.2,16-0.3c2.3,0,4.5-0.1,6.4-0.4c1.9-0.3,3.6-0.5,4.9-0.6c1.4-0.1,2.4-0.2,3.2-0.2c0.7,0,1.1,0,1.1,0
        c1.1,0,2,0.8,2.1,1.9s-0.8,2-1.9,2.1c0,0-0.1,0-0.1,0c0,0-0.4,0-1.1,0c-0.7,0-1.8-0.1-3.2-0.2c-1.4-0.1-3-0.3-4.9-0.6
        c-1.9-0.3-4.1-0.4-6.4-0.4c-4.7-0.1-10.2-0.2-16-0.3C62.5,2.4,56.2,2.4,50,2.4c-12.5,0-25,0-34.4-0.1c-4.7,0-8.6,0-11.3,0
        c-2.7,0-4.3,0-4.3,0c-0.2,0-0.4,0-0.4-0.2C-0.4,2-0.2,2,0,2C0,2,0,2,0,2z"/>
							</g>
						</svg>
					</defs>
				</svg>
			</main>
		);
	}
});

module.exports = VisualizationSection;

/*<a href="#" id="share"><p>Share map</p></a>*/