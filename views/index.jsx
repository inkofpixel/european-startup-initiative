var React = require('react');
var DefaultLayout = require('./layouts/default');
var VisualizationSection = require('./visualization');
var Section = require('./section');

var IndexPage = React.createClass({
	render: function() {
		return (
			<DefaultLayout title={this.props.title}>
				<VisualizationSection/>

				<section id="survey">
				</section>

				<section id="testimonials">
					<div className="wrapper">
						<div className="single">
							<div className="photo mikkoalasaarela"></div>
							<div className="description">
								<div className="name">Mikko Alasaarela</div>
								<div className="position">Founder and CEO of Inbot Inc.</div>
								<div className="quote">“Having lived in Helsinki, London, Berlin and San Francisco, I consider myself a true cosmopolitan, someone who does not wave any specific flag.”</div>
							</div>
						</div>

						<div className="single">
							<div className="description">
								<div className="name align-right">Alex von Frankenberg</div>
								<div className="position align-right">Managing Director High-Tech Gründerfonds</div>
								<div className="quote align-right">"Germany has various startup hotspots and I am convinced, that - thanks to short distances and speedy transportation - they will soon diversify into topical clusters and become equally strong and closely connected."
								</div>
							</div>
							<div className="photo alexfrankenberg"></div>
						</div>

						<div className="single">
							<div className="photo tomwehmeier"></div>
							<div className="description">
								<div className="name">Tom Wehmeier</div>
								<div className="position">Principal & Head of Research at Atomico</div>
								<div className="quote">"We at Atomico believe that interconnectivity between hubs is a key opportunity for Europe to explore and we think that being able to understand the (actual & potential) movement of founders between hubs will be incredibly valuable."
								</div>
							</div>
						</div>

						<div className="single">
							<div className="description">
								<div className="name align-right">Eli David</div>
								<div className="position align-right">Co-Founder and CEO at <a href="http://www.startupblink.com/">StartupBlink.com</a></div>
								<div className="quote align-right">“Europe has such a diverse ecosystem and for me it is thrilling to see how people feel and think about moving to another place, because that is what makes it so much fun!”
								</div>
							</div>
							<div className="photo elidavid"></div>
						</div>

						<div className="single">
							<div className="photo stephanjaquemon"></div>
							<div className="description">
								<div className="name">Stephan Jacquemot</div>
								<div className="position">Principal Microsoft Ventures</div>
								<div className="quote">"What’s great in hubs for startups and investors alike is the concentration of opportunities. But, what exactly is an off-location? If a startup from outside Berlin or London secures VC funding, I deem them even more promising, because they had to get the attention first!"
								</div>
							</div>
						</div>
					</div>
				</section>

				<Section title="project">
					<b>The Startup Heatmap will show</b>
					<ul>
						<li>Which Startup Cities in Europe are the hottest?</li>
						<li>How far does their reputation travel and are they regional or international hotspots?</li>
						<li>Why are people moving to startup in another city?</li>
					</ul>
					<b>Why Do we need this?</b>
					<ul>
						<li>Build awareness of the potentials of Startup Europe</li>
						<li>Provide a reference for startup hotspots to compare against other cities in Europe</li>
						<li>Ignite public debate on the merits of competition of place for Startup Europe</li>
						<li>Provide orientation to investors, startups and people interested in joining a startup</li>
					</ul>
				</Section>

				<Section title="about">
					<p>Hot debate is wrapped around the question where Europe’s hottest startup hub is evolving. Cities like London, Berlin or Stockholm are parading their merits to be viewed as the most innovative spot for startups on the continent.</p>
					<p>Indeed, perception is decisive in this competition as it subconsciously influences the choice of location for both startups and investors. The narrative of the Silicon Valley as the land of opportunity is in a way the prerequisite of its unprecedented triumph. We are therefore mapping the perception of startup hubs in Europe – and how far their reputation travels.</p>
					<p>This allows us both, to compare the more established startup hubs like London and Berlin in their actual reach as well as up-and-coming startup places, which rally to increase their footprint on the map of Startup Europe.</p>
					<p className="last">Our project aims to visualize the positive effects of competition among startup hubs in Europe. By mapping the attractiveness of startup hubs in Europe, we uncover realities of Startup Europe far aside from political window dressing. We believe this will help to guide decision makers as well as members of the community to take action to make Europe an even better place to start a company.</p>
				</Section>

				<Section title="team">
					<div className="profile">ALEXANDER THANNHUBER</div>
					<p>After completing a Bachelor’s Degree of Political Science and Law last year, Alexander has been working as an “Explaining Expert”. Simplifying corporate communication, marketing measures and trainings for Adidas and Johnson {'&'} Johnson. In the coming years, Alexander will zone in on business development as his focus inside and outside of his studies. In his spare time, he often and competitively plays beach volleyball. Even though, with 1,84 cm he is overshadowed by both teammates and opponents.</p>
					<div className="profile">MONIKA KOKŠTAITÈ</div>
					<p>PhD Candidate at Institutions, Markets and Technologies in Lucca, Monika has been working with technology and innovation projects in Brussels in the fields of ICT and energy. From October she is temporary joining DG Growth to work with entrepreneurship in Europe.</p>
					<div className="profile">MARC KEMPF</div>
					<p>Marc Kempf is 23 years old and studied Political Science and Law in Germany and Ecuador. Currently he is doing a Master in Law and Economics in Rotterdam. His hobbies include playing the guitar, scuba diving, Beachvolleyball, and snowboarding. He believes the European Startup Initiative represents a great addition to the existing frameworks dedicated to the European Startup Ecosystem as it seeks to advance our understanding of previously rather unmentioned 2nd tier startup hubs.</p>
					<div className="profile">LUCIA KLINCOVÁ</div>
					<p>Interested ever since in what is happening in the world, it was very natural for Lucia to pursue International Relations studies at the Charles University in Prague. Besides that, she also studied at Université Libre de Bruxelles and completed a summer programs at the Georgetown University and George Mason University. She works as a consultant. Her passions include travelling, music, France, food and sport. She wants to run a marathon one day.</p>
					<div className="profile">Thomas Kösters</div>
					<p>Being a startup program Manager for Microsoft in Germany, Thomas is working with startups on a daily basis. Since more than three years, he is building a ever stronger network of international contacts around innovation and tech. To understand European startups‘ challenges and to motivate aspiring founders to speak out on politics is his goal at esi.</p>
					<div className="profile">Fabrizio Dell’Acqua</div>
					<p>Currently pursuing an academic career with a focus on innovation, business and public policy, Fabrizio is the founder of three technology startups. He is always interested in exploring new ideas, entrepreneurial or scientific ones, and in learning from them. He believes startups in Europe should be more involved in shaping policies that have an impact on them.</p>
					<div className="profile">Johannes Lundborg</div>
					<p>In 2016 Johannes finishes his masters degree in economics as well as a bachelor in business and economics. From 2014 Johannes has a bachelor in political science. Johannes likes analysing and streamlining patterns and processes and has a passion for innovative thinking. In his free time Johannes likes doing sport and to explore food.</p>
				</Section>

				<Section title="contacts">
					<p>Do you wish to watch our projects more closely? We share our thoughts on social media. Make sure you like and follow us!</p>
					<div id="social">
						<a href="https://www.facebook.com/europeanstartupinitiative"><div className="icon facebook"></div></a>
						<a href="https://twitter.com/eustartup"><div className="icon twitter"></div></a>
						<a href="https://www.linkedin.com/grp/home?gid=7418869"><div className="icon linkedin"></div></a>
						<a href="https://www.startus.cc/company/european-startup-initiative"><div className="icon startus"></div></a>
					</div>
					<a href="mailto:info@europeanstartupinitiative.eu?Subject=Startup%20Heatmap" target="_top">Contact us!</a>
				</Section>

				<footer>
					<div id="logo"></div>
					<p>Supported by</p>
					<div id="supporter-container">
						<a href="https://kulturstiftung.allianz.de/en/"><div className="supporter allianz"></div></a>
						<a href="http://www.atomico.com/"><div className="supporter atomico"></div></a>
					</div>

					<p>Design and Development</p>
					<a href="http://www.inkofpixel.com/"><div id="inkofpixel"></div></a>
				</footer>

				<script src="javascripts/startupVisualization.js"></script>
				<script src="javascripts/survey.js"></script>
			</DefaultLayout>
		);
	}
});


module.exports = IndexPage;