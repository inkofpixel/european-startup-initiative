var React = require('react');

var NavigationBar = React.createClass({
	render: function() {
		var anchors = this.props.anchors.map(function(anchor) {
			var link = "/#" + anchor.toLowerCase().replace(" ", "-");
			return <li key={anchor}><a className="anchor" href={link}>{anchor}</a></li>
		});
		return (
			<nav className="navigation-bar">
				<div id="logo"></div>
				<ul>{anchors}</ul>
			</nav>
		);
	}
});

module.exports = NavigationBar;
