var React = require('react');

var NavigationBar = require('../components/NavigationBar');

var DefaultLayout = React.createClass({

	fbPixelCodeScript: function() {
		return "!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?" +
				"n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;" +
				"n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;" +
				"t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window," +
				"document,'script','//connect.facebook.net/en_US/fbevents.js');" +
				"fbq('init', '1163220533705668');" +
				"fbq('track', 'PageView');";
	},

	fbPixelCodeNoScript: function() {
		return <img height='1' width='1' style='display:none' src='https://www.facebook.com/tr?id=1163220533705668&ev=PageView&noscript=1'/>;
	},

	render: function() {
		var sections = ["Heat map", "Survey", "Project", "About", "Team", "Contacts"];
		var googleAnalytics = "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-28251380-3', 'auto');ga('send', 'pageview');";
		return (
			<html>
			<head>
				<title>{this.props.title}</title>
				<meta charSet="utf-8"/>

				<meta property="og:image"
				      content="http://www.startupheatmap.eu/images/og-facebook.png" />

				<link rel="shorcut icon" href="images/favicon.png" type="favicon"/>
				<link rel="stylesheet" type="text/css" href="/stylesheets/bundle/style.css"/>
				<link rel="stylesheet" type="text/css" href="/stylesheets/vis.css"/>

				<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADHp-4QAbBUpSy_vn1VrP1RpDUo0Eg5XI&signed_in=true&libraries=places"></script>
				<script src="/javascripts/react.js"></script>
				<script src="/javascripts/d3.js"></script>
				<script src="/javascripts/topojson.js"></script>

				<script src="/javascripts/navigation.js"></script>

				<script dangerouslySetInnerHTML={{__html: googleAnalytics}}></script>
				<script dangerouslySetInnerHTML={{__html: this.fbPixelCodeScript()}}></script>
			</head>
			<body>
			<div>
				<img height="1" width="1" style={{display: 'none'}} src='https://www.facebook.com/tr?id=1163220533705668&ev=PageView&noscript=1'/>
			</div>
			<header>
				<NavigationBar logo={"images/logo.svg"} anchors={sections}/>
			</header>
			{this.props.children}
			</body>
			</html>
		);
	}
});

module.exports = DefaultLayout;
