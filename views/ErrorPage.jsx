/**
 * @author Mattia Marcon
 * @created 20/09/15
 *
 * Copyright inkOfPixel 2015.
 */

var React = require('react');

var NavigationBar = require('./components/NavigationBar');

var ErrorPage = React.createClass({
	render: function () {
		var sections = ["Heat map", "Project", "Survey", "About", "Team", "Contacts"];

		return (
			<html>
			<head>
				<title>{this.props.title}</title>
				<meta charSet="utf-8"/>

				<link rel="shorcut icon" href="/images/favicon.png" type="favicon"/>
				<link rel="stylesheet" type="text/css" href="/stylesheets/bundle/style.css"/>
			</head>
			<body>
			<header>
				<NavigationBar logo={"images/logo.svg"} anchors={sections}/>
			</header>
			<div id="error-outer-wrapper">
				<div id="error">
					<div className="description">
						<h2>{this.props.error.status}</h2>

						<h1>something has broken</h1>
						<a className="call-to-action" href="/">Put in order</a>
					</div>
				</div>
			</div>


			</body>
			</html>
		);
	}
});

module.exports = ErrorPage;

/* <h1>{this.props.message}</h1> */