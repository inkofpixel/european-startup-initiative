var React = require('react');

var Section = React.createClass({
	render: function() {
		return (
			<section id={this.props.title.toLowerCase().replace(" ", "-")}>
				<div className="wrapper">
					<h1>{this.props.title}</h1>
					<div className="content">{this.props.children}</div>
				</div>
			</section>
		);
	}
});

module.exports = Section;